package com.gitlab.johnjvester.rollbar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RollbarApplication {
    public static void main(String[] args) {
        SpringApplication.run(RollbarApplication.class, args);
    }
}
