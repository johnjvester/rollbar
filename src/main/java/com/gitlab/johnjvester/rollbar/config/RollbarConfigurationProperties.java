package com.gitlab.johnjvester.rollbar.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration("rollbarConfiguration")
@ConfigurationProperties("rollbar")
public class RollbarConfigurationProperties {
    private String accessToken;
    private String branch;
    private String codeVersion;
    private String environment;
}
