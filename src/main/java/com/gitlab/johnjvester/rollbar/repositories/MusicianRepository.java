package com.gitlab.johnjvester.rollbar.repositories;

import com.gitlab.johnjvester.rollbar.entities.Musician;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MusicianRepository extends JpaRepository<Musician, Long> { }
