package com.gitlab.johnjvester.rollbar.utils;

import com.gitlab.johnjvester.rollbar.models.RollbarExceptionData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class RollbarException extends Exception {
    private String message;
    private RollbarExceptionData rollbarExceptionData;
}
