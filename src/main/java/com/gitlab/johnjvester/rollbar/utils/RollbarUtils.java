package com.gitlab.johnjvester.rollbar.utils;

import com.gitlab.johnjvester.randomizer.RandomGenerator;
import com.gitlab.johnjvester.rollbar.models.RollbarExceptionData;

import java.util.ArrayList;
import java.util.List;

public final class RollbarUtils {
    private RollbarUtils() { }

    public static RollbarExceptionData createRollbarExceptionData() {
        RandomGenerator<RollbarExceptionData> randomGenerator = new RandomGenerator<>();
        List<RollbarExceptionData> randomItemList = randomGenerator.randomize(getRollBarExceptionData(), new Integer("1"));
        return randomItemList.get(0);
    }

    public static String createRandomError() {
        List<String> errorList = new ArrayList<>();
        errorList.add("Bad Request #1");
        errorList.add("Bad Request #2");
        errorList.add("Bad Request #3");
        errorList.add("Bad Request #4");
        errorList.add("Bad Request #5");

        RandomGenerator<String> randomGenerator = new RandomGenerator<>();
        List<String> randomItemList = randomGenerator.randomize(errorList, new Integer("1"));
        return randomItemList.get(0);
    }

    private static List<RollbarExceptionData> getRollBarExceptionData() {
        List<RollbarExceptionData> rollbarExceptionDataList = new ArrayList<>();

        RollbarExceptionData rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(1L);
        rollbarExceptionData.setUserName("john.doe");
        rollbarExceptionData.setUserEmail("john.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(2L);
        rollbarExceptionData.setUserName("jane.doe");
        rollbarExceptionData.setUserEmail("jane.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(3L);
        rollbarExceptionData.setUserName("bob.doe");
        rollbarExceptionData.setUserEmail("bob.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(4L);
        rollbarExceptionData.setUserName("sallie.doe");
        rollbarExceptionData.setUserEmail("sallie.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(5L);
        rollbarExceptionData.setUserName("darren.doe");
        rollbarExceptionData.setUserEmail("darren.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(6L);
        rollbarExceptionData.setUserName("tom.doe");
        rollbarExceptionData.setUserEmail("tom.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(7L);
        rollbarExceptionData.setUserName("nicole.doe");
        rollbarExceptionData.setUserEmail("nicole.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(8L);
        rollbarExceptionData.setUserName("sydney.doe");
        rollbarExceptionData.setUserEmail("sydney.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(9L);
        rollbarExceptionData.setUserName("eric.doe");
        rollbarExceptionData.setUserEmail("eric.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(10L);
        rollbarExceptionData.setUserName("phoenix.doe");
        rollbarExceptionData.setUserEmail("phoenix.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(11L);
        rollbarExceptionData.setUserName("jackie.doe");
        rollbarExceptionData.setUserEmail("jackie.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(12L);
        rollbarExceptionData.setUserName("russell.doe");
        rollbarExceptionData.setUserEmail("russell.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(13L);
        rollbarExceptionData.setUserName("max.doe");
        rollbarExceptionData.setUserEmail("max.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(14L);
        rollbarExceptionData.setUserName("gretchen.doe");
        rollbarExceptionData.setUserEmail("grethen.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(15L);
        rollbarExceptionData.setUserName("deby.doe");
        rollbarExceptionData.setUserEmail("deby.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(16L);
        rollbarExceptionData.setUserName("dave.doe");
        rollbarExceptionData.setUserEmail("dave.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        rollbarExceptionData = new RollbarExceptionData();
        rollbarExceptionData.setUserId(17L);
        rollbarExceptionData.setUserName("danyel.doe");
        rollbarExceptionData.setUserEmail("danyel.doe@example.com");
        rollbarExceptionDataList.add(rollbarExceptionData);

        return rollbarExceptionDataList;
    }
}
