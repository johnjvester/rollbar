package com.gitlab.johnjvester.rollbar.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RollbarExceptionData {
    private static final String USER_ID = "userId";
    private static final String USER_NAME = "userName";
    private static final String USER_EMAIL = "email";
    private static final String IP_ADDRESS = "ipAddress";
    private static final String REQUEST_TYPE = "requestType";
    private static final String URI = "uri";

    private Long userId;
    private String userName;
    private String userEmail;
    private String ipAddress;
    private String requestType;
    private String uri;

    public Map<String, Object> getRollbarMap() {
        Map<String, Object> rollbarMap = new HashMap<>();

        if (userId != null) {
            rollbarMap.put(USER_ID, userId);
        }

        if (StringUtils.isNotEmpty(userName)) {
            rollbarMap.put(USER_NAME, userName);
        }

        if (StringUtils.isNotEmpty(userEmail)) {
            rollbarMap.put(USER_EMAIL, userEmail);
        }

        if (StringUtils.isNotEmpty(ipAddress)) {
            rollbarMap.put(IP_ADDRESS, ipAddress);
        }

        if (StringUtils.isNotEmpty(requestType)) {
            rollbarMap.put(REQUEST_TYPE, requestType);
        }

        if (StringUtils.isNotEmpty(uri)) {
            rollbarMap.put(URI, uri);
        }

        return rollbarMap;
    }
}
