package com.gitlab.johnjvester.rollbar.events;

import com.gitlab.johnjvester.rollbar.config.RollbarConfigurationProperties;
import com.gitlab.johnjvester.rollbar.utils.SecurityUtils;
import com.rollbar.notifier.Rollbar;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Slf4j
@Component
public class RollbarEvents {
    private final Environment environment;
    private final RollbarConfigurationProperties rollbarConfigurationProperties;
    private final Rollbar rollbar;

    @PostConstruct
    public void postConstruct() {
        log.info("Started {} on port #{} using Rollbar accessToken={} ({})",
                environment.getProperty("spring.application.name"),
                environment.getProperty("server.port"),
                SecurityUtils.maskCredentialsRevealPrefix(rollbarConfigurationProperties.getAccessToken(), 5, '*'),
                rollbarConfigurationProperties.getEnvironment());
        rollbar.info(String.format("Started %s on port #%s using Rollbar accessToken=%s (%s)",
                environment.getProperty("spring.application.name"),
                environment.getProperty("server.port"),
                SecurityUtils.maskCredentialsRevealPrefix(rollbarConfigurationProperties.getAccessToken(), 5, '*'),
                rollbarConfigurationProperties.getEnvironment()));
    }
}
