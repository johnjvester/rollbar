package com.gitlab.johnjvester.rollbar.services;

import com.gitlab.johnjvester.rollbar.entities.Musician;
import com.gitlab.johnjvester.rollbar.repositories.MusicianRepository;
import com.gitlab.johnjvester.rollbar.utils.RollbarUtils;
import com.rollbar.notifier.Rollbar;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class MusicianService {
    private final MusicianRepository musicianRepository;
    private final Rollbar rollbar;

    public List<Musician> getAllMusicians() {
        List<Musician> musicians = musicianRepository.findAll();

        if (CollectionUtils.isNotEmpty(musicians)) {
            rollbar.info(String.format("Found %s musicians", musicians.size()), RollbarUtils.createRollbarExceptionData().getRollbarMap());
        } else {
            rollbar.warning("Could not locate any musicians", RollbarUtils.createRollbarExceptionData().getRollbarMap());
        }

        return musicians;
    }

    public Musician postMusician(Musician musician) throws Exception {
        log.debug("postMusician(musician={})", musician);
        throw new Exception("Not accepting new musicians at this time.");
    }

    public Musician getIndexOutOfBounds() {
        List<Musician> musicianList = new ArrayList<>();
        return musicianList.get(0);
    }
}
