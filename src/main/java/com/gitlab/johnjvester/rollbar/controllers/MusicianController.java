package com.gitlab.johnjvester.rollbar.controllers;

import com.gitlab.johnjvester.rollbar.entities.Musician;
import com.gitlab.johnjvester.rollbar.services.MusicianService;
import com.gitlab.johnjvester.rollbar.utils.RollbarException;
import com.gitlab.johnjvester.rollbar.utils.RollbarUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin
@Controller
@RequestMapping(value = "/musicians", produces = MediaType.APPLICATION_JSON_VALUE)
public class MusicianController {
    private final MusicianService musicianService;

    @GetMapping
    public ResponseEntity<List<Musician>> getAllMusicians() {
        return new ResponseEntity<>(musicianService.getAllMusicians(), HttpStatus.OK);
    }

    @GetMapping("/badRequest")
    public ResponseEntity<Musician> getException() throws Exception {
        throw new RollbarException(RollbarUtils.createRandomError(), RollbarUtils.createRollbarExceptionData());
    }

    @GetMapping("/indexOutOfBounds")
    public ResponseEntity<Musician> getIndexOutOfBounds() {
        return new ResponseEntity<>(musicianService.getIndexOutOfBounds(), HttpStatus.ACCEPTED);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Musician> postMusician(@RequestBody Musician musician) throws Exception {
        try {
            return new ResponseEntity<>(musicianService.postMusician(musician), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new RollbarException(e.getMessage(), RollbarUtils.createRollbarExceptionData());
        }
    }
}
