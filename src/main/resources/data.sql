-- Instruments
INSERT INTO Instrument (id, name) VALUES (1, 'Guitar');
INSERT INTO Instrument (id, name) VALUES (2, 'Bass');
INSERT INTO Instrument (id, name) VALUES (3, 'Keyboards');
INSERT INTO Instrument (id, name) VALUES (4, 'Drums');
INSERT INTO Instrument (id, name) VALUES (5, 'Vocals');

-- Musicians
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (1, 'Alex', 'Lifeson', 1);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (2, 'Trevor', 'Rabin', 1);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (3, 'Geddy', 'Lee', 2);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (4, 'Chris', 'Squire', 2);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (5, 'Tony', 'Kaye', 3);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (6, 'Neil', 'Peart', 4);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (7, 'Alan', 'White', 4);
INSERT INTO Musician (id, first_name, last_name, instrument_id) VALUES (8, 'Jon', 'Anderson', 5);
