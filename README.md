# Rollbar Spring Boot Example Application (`rollbar`)

[![pipeline status](https://gitlab.com/johnjvester/rollbar/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/rollbar/commits/master)

> The Rollbar Spring Boot Example Application (`rollbar`) is a [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) based application, utilizing the [Spring Boot](<https://spring.io/projects/spring-boot>) framework and is intended to provide a working example of [Rollbar](http://www.rollbar.com) usage.

# Publications

This repository is related to an article published on DZone.com:

* [Discovering Rollbar as a Spring Boot Developer](https://dzone.com/articles/discovering-rollbar-as-a-spring-boot-developer)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Configuration Information

The following elements need to be set in order to run the `rollbar` application:

* `${ROLLBAR_ACCESS-TOKEN}` - Access Token to write to Rollbar service `[rollbar.access-token]`
* `${ROLLBAR_CODE-VERSION}` - Code version to utilize (Git SHA) `[rollbar.code-version]`
* `${PORT}` - server port to use `[server.port]`

## Example Data

The Rollbar Spring Boot Example Application (`rollbar`) has a very simple data set using an in-memory H2 database.
 
There are a handful of URIs to demonstrate using the Rollbar service.

### `/musicians` (GET)

The `/musicians` (GET) URI will return a list of example musicians, from the classic rock-n-roll bands [Rush](https://en.wikipedia.org/wiki/Rush_(band)) and [Yes](https://en.wikipedia.org/wiki/Yes_(band)).  This process will perform a debug log into the Rollbar service.

### `/musicians/badRequest` (GET)

The `/musicians/badRequest` (GET) URI will throw an exception to generate a bad request, logging information in the Rollbar service.

### `/musicians/indexOutOfBounds` (GET)

The `/musicians/indexOutOfBounds` (GET) URI will throw a 500 exception, logging information in the Rollbar service.

### `/musicians` (POST)

The `/musicians` (POST) URL will throw an exception to indicate no new musicians can be added at this time.

## Logging Core Events

When the Rollbar Spring Boot Example Application (`rollbar`) starts, an information event will be logged to the Rollbar service - using the `RollbarServletContextListener` class.

## Custom Data In Rollbar

The Rollbar Spring Boot Example Application (`rollbar`) includes some classes to provide additional information for this example:

* the `RollbarException` class extends the Java `Exception` class to include `RollbarExceptionData`
* the `RollbarExceptionData` class contains custom attributes that included with the data sent to Rollbar.  In an actual system, this would likely contain real end-user information.
* the `RollbarUtils` class uses the [`random-generator`](https://gitlab.com/johnjvester/RandomGenerator) library to randomly pick one of 17 static `RollbarExceptionData` instances.

Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.